# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2010 Associazione OpenERP Italia
#    (<http://www.openerp-italia.org>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import netsvc
import pooler, tools


from openerp.osv import orm, fields

class stock_picking_out(orm.Model):
    #_inherit = 'stock.picking.out'
    _name = 'stock.picking.out'
    _inherit = 'stock.picking'
    _table = 'stock_picking'

    _columns =  {
        'invoice_ids': fields.many2many('account.invoice',
                                    'invoice_stock_rel',
                                    'invoice_id',
                                    'picking_out_id',
                                    'Fatture associate')

       # 'ddt_ids': fields.many2many('stock.picking.out',
       #                              'invoice_stock_rel',
       #                              'picking_out_id',
       #                              'invoice_id',
       #                              'D.D.T. associati')


    }


stock_picking_out()